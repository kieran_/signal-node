signal-node
====

signal-node is a project by Kieran Harkin. This project will contain the server signaling node for Noelle Grant's website

Installation
--------------

```sh
git clone --recursive https://kieran_@bitbucket.org/kieran_/signal-node.git
cd 0x
sudo apt-get install libssl-dev g++ cmake
sudo make build
sudo make

```

Thanks to
-----------

signal-node uses a few other projects to help it get the job done:

* [libwebsockets] - helps us connect to the users via websockets
* [json11] - great for reading what the clients are saying and used to talk to them also
* [sole] - UUID generator
* [jQuery] - js library that everybody should know

[libwebsockets]:http://libwebsockets.org/trac/libwebsockets
[json11]:https://github.com/dropbox/json11/
[jQuery]:http://jquery.com/
[sole]:https://github.com/r-lyeh/sole

signal-node was forked from [0x]
[0x]:https://bitbucket.org/kieran_/0x
