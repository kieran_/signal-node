#include "packet.h"

using namespace Ox;

Packet::Packet(string json, Player* player, Universe* verse){
	string err;
	Json data = Json::parse(json, err);
	if (!err.empty()) {
		lwsl_notice("Parse error for packet: %s\n", err.c_str());
		this->error = true;
		this->reply = "Internal server error";
	} else {
		code = (PacketCode)data["code"].int_value();
		this->error = false;
							
		//handle packets
		switch(code){
			case PLAYER_JOIN: {
				player->setName(data["data"]["username"].string_value());
				break;
			}
			case PLAYER_MOVE:{
				if(inVerse(player)){
					player->moveTo(data["data"]["x"].number_value(), data["data"]["y"].number_value(), data["data"]["z"].number_value());
					
					Json rsp = Json::object{
						{ "player", player->getUUID().str() },
						{ "code", code },
						{ "data", data["data"] }
					};
					
					this->broadcast = rsp.dump();
				}else{
					this->reply = "You are not in a universe!";
					this->error = true;
				}
				break;
			}
			default:{
				lwsl_notice("Unknown packet #%s\n", code);
			}
		}
	}
}

bool Packet::inVerse(Player* p){
	if(p->getVerse())
		return true;
	return false;
}
