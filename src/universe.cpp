#include "universe.h"

using namespace Ox;

void Universe::playerJoin(Player* p){
	p->setVerse(this);
	
	//notify other players
	Json pj = Json::object{
		{ "code", (int)PacketCode::PLAYER_JOIN },
		{ "uuid", p->getUUID().str() },
		{ "name", p->getName() }
	};
	string msg = pj.dump();
	
	broadcast((unsigned char*)msg.c_str(), msg.length());
	
	//send messages to this new player about whos in this universe
	for(Player* pl : this->players){
		pj = Json::object{
			{ "code", (int)PacketCode::PLAYER_JOIN },
			{ "uuid", pl->getUUID().str() },
			{ "name", pl->getName() }
		};
		
		msg = pj.dump();
		libwebsocket_write(p->getWSI(), (unsigned char*)msg.c_str(), msg.length(), LWS_WRITE_TEXT);
	}
	
	this->players.push_back(p);
}

bool Universe::playerLeave(Player* p){
	bool ok = false;
	int i = 0;
	for(i = 0; i < this->players.size(); i++){
		if(this->players[i]->getUUID() == p->getUUID())
			ok = true; break;
	}
	
	if(ok)
		this->players.erase(this->players.begin() + i);
		
	return ok;
}
void Universe::broadcast(unsigned char* in, size_t len){
	for(Player* pl : *(this->getPlayers())){
		libwebsocket_write(pl->getWSI(), in, len, LWS_WRITE_TEXT);
	}
}
