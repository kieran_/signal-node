#ifndef SERVER_H
#define SERVER_H

#include <thread>
#include <cstring>
#include <vector>
#include <iostream>

#include "queue.h"
#include "packet.h"
#include "config.h"
#include "universe.h"

#include "../libwebsockets/lib/libwebsockets.h"
#include "../json11/json11.hpp"

using namespace Ox;
using std::thread;
using std::string;
using std::vector;
using json11::Json;

namespace Ox {
		
	struct session_data{
		libwebsocket* wsi;
		Player* p;
	};
	
	class Server{
	public:
		Server() : proto{{"http", callback_http, 0 },{ "0x", callback, sizeof(session_data) },{NULL,NULL,0}} {}
		~Server();
		static int callback(struct libwebsocket_context *ctx, struct libwebsocket *wsi, enum libwebsocket_callback_reasons reason, void *user, void *in, size_t len);
		static int callback_http(struct libwebsocket_context *ctx, struct libwebsocket *wsi, enum libwebsocket_callback_reasons reason, void *user, void *in, size_t len);
		bool init(Config* cfg);
		bool restart();
		void stop();
		void run();
		void runConsume();
		
		static vector<Universe>* verse;
		static Queue<Packet>* q;
	private:
		thread main;
		thread consumer;
		bool isRunning;
		struct libwebsocket_protocols proto[3];
		struct lws_context_creation_info info;
		struct libwebsocket_context *context;
		Config* cfg;
	};
}
#endif
