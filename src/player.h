#ifndef PLAYER_H
#define PLAYER_H

#include <string>

#include "../libwebsockets/lib/libwebsockets.h"
#include "../sole/sole.hpp"

using std::string;

namespace Ox{
	class Universe;
	
	class Player{
	public:
		Player(libwebsocket* w, string n) : wsi(w), name(n), id(sole::uuid4()), verse(0) {}
		
		void setVerse(Universe* v);
		bool moveTo(double x, double y, double z);
		
		libwebsocket* getWSI() { return this->wsi; }
		Universe* getVerse() { return this->verse; }
		string getName() { return this->name; }
		void setName(string n) { this->name = n; }
		sole::uuid getUUID() { return id; }
		
	private:
		sole::uuid id;
		libwebsocket* wsi;
		Universe* verse;
		string name;
		int ip;
		double x;
		double y;
		double z;
		int hp;
	};
}
#endif
