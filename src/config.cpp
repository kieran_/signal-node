#include "config.h"

using std::ifstream;
using std::ofstream;
using std::stringstream;
using Ox::Config;
using json11::Json;

void Config::load() {
	ifstream f(filename);
	if(f){
		stringstream buffer;
		buffer << f.rdbuf();
		
		string err;
		auto json = Json::parse(buffer.str(), err);
		if (!err.empty()) {
			lwsl_notice("Failed to load config file: %s", err.c_str());
		} else {
			nodeName = json["node_name"].string_value();
			nodeMaster = json["node_master"].string_value();
			iface = json["iface"].string_value();
			port = json["port"].int_value();
			lwsl_notice("Config file loaded: %s\n",json.dump().c_str());
		}
	}else{
		lwsl_notice("No config file found, dumping default config to server.cfg\n");
		save();
	}
}

void Config::save(){
	//Dump current settings to disk
	ofstream f(filename, ofstream::out);
	
	Json s = Json::object {
		{"node_name", nodeName},
		{"node_master", nodeMaster},
		{"iface", iface},
		{"port", port}
	};
	
	f << s.dump();
	f.close();
}
