#ifndef PACKET_H
#define PACKET_H

#include "player.h"
#include "universe.h"

#include "../json11/json11.hpp"
#include "../libwebsockets/lib/libwebsockets.h"

using json11::Json;
using std::string;

namespace Ox{
	enum PacketCode {
		DEBUG,
		INFO,
		WARN,
		ERROR,
		PLAYER_JOIN,
		PLAYER_LEAVE,
		PLAYER_CHAT,
		PLAYER_MOVE
	};

	class Packet{
	public:
		Packet(string json,Player* player, Universe* univ);
		PacketCode code;
		string getReplyResponse() { return reply; }
		string getBroadcastResponse() { return broadcast; }
		bool wasError() { return error; }
	private:
		bool inVerse(Player* p);
		
		string reply;
		string broadcast;
		bool error;
	};
}
#endif
