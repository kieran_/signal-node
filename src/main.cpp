#include <thread>
#include <iostream>
#include "queue.h"
#include "server.h"

using namespace Ox;
using namespace std;

typedef struct {
	const char *name;
	const char *help;
	function<void ()> go;
} cmd;

Server serv;
Config cfg;

int main(){
	cfg.load();
	if (serv.init(&cfg)){
		cmd commands[] = {
			{"q", "Stops the server", [&] { }},
			{"help", "Shows this message", [&] { }},
			{"reload", "Restarts the webs socket server", [&] { serv.restart(); }},
			{"info", "Displays player information and status", [&] {}}
		};

		string incmd;
		bool foundCMD = false;
		do {
			foundCMD = false;
			cout << ">";
			getline(cin,incmd);
			
			if(incmd.compare("help") == 0){
				foundCMD = true;
				cout << "0x version: " << OX_VERSION << endl;
				cout << "Copyright(c) Kieran Harkin 2014\n\nBelow is a list of possible commands:" << endl;
				
				for(cmd c : commands){
					cout << "\t" << c.name << "\t" << c.help << endl;
				}
			}else{
				for(cmd c : commands){
					if(incmd.find(c.name) != string::npos){
						c.go();
						foundCMD = true;
						break;
					}
				}
			}
			
			if(!foundCMD){
				cout << incmd << ": command not found" << endl;
			}
			
		}while(incmd.compare("q") != 0);
		
		serv.stop();
	}
	return 0;
}
