# Makefile v0.1

CC=g++
BIN=sn
BIN_DEBUG=sn_debug
LIBS=-lwebsockets
LIBS_DIR=-Llibwebsockets/build/lib
SRC=json11/json11.cpp sole/sole.cpp src/*.cpp
DEF=-DSN_VERSION="\"$(SN_VERSION)\"" -DLWSL_VERSION="\"$(LWSL_VERSION)\""
FLAGS=-O2 -std=c++11 -pthread -Wl,--no-as-needed

SN_VERSION= $(shell git log --pretty=format:'%h (%ad)' --date=short -n 1)
LWSL_VERSION= $(shell cd libwebsockets && git log --pretty=format:'%h (%ad)' --date=short -n 1)

all:
	$(CC) $(FLAGS) -o$(BIN) $(SRC) $(LIBS_DIR) $(LIBS) $(DEF)

debug:
	$(CC) -g $(FLAGS) -o$(BIN_DEBUG) $(SRC) $(LIBS_DIR) $(LIBS) $(DEF)

build:
	mkdir -p libwebsockets/build
	cd libwebsockets/build; cmake -DLIB_SUFFIX=64 -DLWS_WITHOUT_CLIENT=ON ..; make
	cp -rf libwebsockets/build/lib/* /usr/lib

clean:
	rm -rf sn
	rm -rf libwebsockets/build
